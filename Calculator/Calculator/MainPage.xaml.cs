﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        double variable1;
        double variable2;
        string answer;
        int? operation;

        public MainPage()
        {
            InitializeComponent();
        }

        private void updateText(int num)
        {
            if (operation == null)
            {
                variable1 = (variable1 * 10) + num;
                calculatorVar.Text = variable1.ToString();
                history.Text = variable1.ToString();
            }
            else
            {
                variable2 = (variable2 * 10) + num;
                calculatorVar.Text = variable2.ToString();

                switch (operation)
                {
                    case 0: // addition
                        history.Text = variable1.ToString() + "+" + variable2.ToString();
                        break;
                    case 1: // subtraction
                        history.Text = variable1.ToString() + "-" + variable2.ToString();
                        break;
                    case 2:
                        history.Text = variable1.ToString() + "*" + variable2.ToString();
                        break;
                    case 3:
                        history.Text = variable1.ToString() + "/" + variable2.ToString();
                        break;
                    default:
                        break;
                }
            }
        }

        private void onCancelClicked(object sender, EventArgs e)
        {
            variable1 = 0;
            variable2 = 0;
            operation = null;

            calculatorVar.Text = "0";
            history.Text = "";
        }

        // using variable1 and variable2 that are stores along with
        // the operation value, calculates the answer 
        private void onEqualClicked(object sender, EventArgs e)
        {
            switch (operation)
            {
                case 0: // addition
                    answer = (variable1 + variable2).ToString();
                    calculatorVar.Text = answer;
                    break;
                case 1: // subtraction
                    answer = (variable1 - variable2).ToString();
                    calculatorVar.Text = answer;
                    break;
                case 2: // multiplication
                    answer = (variable1 * variable2).ToString();
                    calculatorVar.Text = answer;
                    break;
                case 3: // division
                    try
                    {
                        answer = (variable1 / variable2).ToString();
                        calculatorVar.Text = answer;
                    }
                    catch (DivideByZeroException errorDiv)
                    {
                        Console.WriteLine(errorDiv.Message);
                        calculatorVar.Text = "ERROR: DIV 0";
                    }
                    break;
                default:
                    calculatorVar.Text = "ERROR";
                    break;
            }

            variable1 = 0;
            variable2 = 0;
            operation = null;
            history.Text = "";
        }

        // adding has an operation value of 0
        private void onAddClicked(object sender, EventArgs e)
        {
            operation = 0;
            calculatorVar.Text = "+";
            history.Text = variable1.ToString() + "+" + variable2.ToString();
        }

        // subtraction has an operation value of 1
        private void onSubClicked(object sender, EventArgs e)
        {
            operation = 1;
            calculatorVar.Text = "-";
            history.Text = variable1.ToString() + "-" + variable2.ToString();
        }

        // multiplication has an operation value of 2
        private void onMultClicked(object sender, EventArgs e)
        {
            operation = 2;
            calculatorVar.Text = "*";
            history.Text = variable1.ToString() + "*" + variable2.ToString();
        }

        // division has an operation value of 3
        private void onDivClicked(object sender, EventArgs e)
        {
            operation = 3;
            calculatorVar.Text = "/";
            history.Text = variable1.ToString() + "/" + variable2.ToString();
        }

        // list of functions that will send updateText a number
        private void on0Clicked(object sender, EventArgs e)
        {
            updateText(0);
        }
        private void on1Clicked(object sender, EventArgs e)
        {
            updateText(1);
        }
        private void on2Clicked(object sender, EventArgs e)
        {
            updateText(2);
        }
        private void on3Clicked(object sender, EventArgs e)
        {
            updateText(3);
        }
        private void on4Clicked(object sender, EventArgs e)
        {
            updateText(4);
        }
        private void on5Clicked(object sender, EventArgs e)
        {
            updateText(5);
        }
        private void on6Clicked(object sender, EventArgs e)
        {
            updateText(6);
        }
        private void on7Clicked(object sender, EventArgs e)
        {
            updateText(7);
        }
        private void on8Clicked(object sender, EventArgs e)
        {
            updateText(8);
        }
        private void on9Clicked(object sender, EventArgs e)
        {
            updateText(9);
        }
    }
}